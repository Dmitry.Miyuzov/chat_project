package utils;

import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

/*
This class uses for hashing and parsing for password.
 */
public class EncoderPassword {

    private static final Logger logger = Logger.getLogger(EncoderPassword.class);

    private EncoderPassword(){}

    public static String hashPassword(String openedPassword) {
        int workload = 12;
        String salt = BCrypt.gensalt(workload);

        return BCrypt.hashpw(openedPassword, salt);
    }

    public static boolean checkPassword(String openedPassword, String storedHash) {
        boolean isPasswordVerified;

        if (null == storedHash || !storedHash.startsWith("$2a$")) {
            logger.error("Invalid hash provided for comparison");
        }

        isPasswordVerified = BCrypt.checkpw(openedPassword, storedHash);

        return isPasswordVerified;
    }

}
