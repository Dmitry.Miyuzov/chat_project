package DAO;

import DTO.Token;
import DTO.User;

import java.util.List;


public interface UserDAO {

    String getEmail(String email);

    int getIdUser(String email);

    String getPassword(String email);

    void addUserToDataBase(User user);

    void addTokenToDataBase(Token token);

    void updatePassword(int id, String pass);

    int getRole(String role);

    User getUser(int id);

    List<User> getAllOnlineUsers();
}
