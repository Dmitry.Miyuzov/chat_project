function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function(e) {
            $('#registration-image').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}
$(document).ready(function () {
    $(idFile).change(function() {
        let file = this.files[0];
        if (file === undefined) {
            $(idRegistrationImage).attr(textSrc, "#");
        } else {
            if (!file.type.match('image.*')) {
                Swal.fire({
                    icon: "error",
                    title: 'Данный формат не поддерживается! Пожалуйста, выберите другой файл.',
                    confirmButtonText: "Ок",
                    confirmButtonColor: "#6666ff",
                    allowEscapeKey: false,
                    position: 'top',
                });
                $(idRegistrationImage).attr('src', "#");
            } else {
                readURL(this);
            }
        }
    });
});
