package DTO;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;

public class Message {
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private int idMessage;
    private User user;
    private LocalDateTime localDateTime;
    private String communication;
    private Status status;

    public Message(User user, LocalDateTime localDateTime, String communication, Status status) {
        this.user = user;
        this.localDateTime = localDateTime;
        this.communication = communication;
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setIdMessage(int idMessage) {
        this.idMessage = idMessage;
    }

    public int getIdMessage() {
        return idMessage;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLocalDateTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.ENGLISH);

        return localDateTime.format(dtf);
    }

    public String getCommunication() {
        return communication;
    }

    public Status getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Message message = (Message) o;
        return Objects.equals(user, message.user) &&
                Objects.equals(localDateTime, message.localDateTime) &&
                Objects.equals(communication, message.communication) &&
                status == message.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, localDateTime, communication, status);
    }

    @Override
    public String toString() {
        return "Message{" +
                "user=" + user +
                ", gregorianCalendar=" + localDateTime +
                ", communication='" + communication + '\'' +
                ", status=" + status +
                '}';
    }

}
