package DTO;

public enum Gender {
    MALE,
    FEMALE;

    private static final String GENDER_MALE = "male";
    private static final String GENDER_FEMALE = "female";


    public static Gender getGender(String titleGender) {
        Gender gender;
        if (GENDER_MALE.equals(titleGender)) {
            gender = Gender.MALE;
        } else if (GENDER_FEMALE.equals(titleGender)) {
            gender = Gender.FEMALE;
        } else {
            gender = null;
        }
        return gender;
    }
}
