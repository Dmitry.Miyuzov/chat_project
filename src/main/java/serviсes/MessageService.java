package serviсes;

import DAO.MessageDAO;
import DAO.UserDAO;
import DTO.Message;
import DTO.Status;
import DTO.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import utils.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageService implements Service {
    private static final int STATUS_200 = 200;
    private static final int STATUS_422 = 422;
    private static final String PARAMETER_LAST_MESSAGE = "lastMessage";
    private static final String PARAMETER_LIMIT = "limit";
    private static final String METHOD_GET = "GET";
    private static final String METHOD_POST = "POST";
    private static final String PROPERTY_ID_USER = "idUser";
    private static final String PROPERTY_COMMUNICATION = "communication";
    private static final String PROPERTY_ID_MESSAGE = "idMessage";
    private static final String PROPERTY_NAME = "name";
    private static final String PROPERTY_DATE = "date";
    private static final String PROPERTY_TITLE_ROLE = "titleRole";

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        String method = req.getMethod();
        if (method.equals(METHOD_GET)) {
            if (req.getParameter(PARAMETER_LAST_MESSAGE) == null) {
                giveLastMessages(req, resp);
            } else {
                giveHistoryMessages(req, resp);
            }
        } else if (method.equals(METHOD_POST)) {
            saveMessage(req,resp);
        }
    }

    private void giveHistoryMessages(HttpServletRequest request, HttpServletResponse response) {
        int count = Integer.parseInt(request.getParameter(PARAMETER_LIMIT));
        int withIdMessage = Integer.parseInt(request.getParameter(PARAMETER_LAST_MESSAGE));
        MessageDAO messageDAO = ServletUtils.getMessageDao(request);
        List<Message> list = messageDAO.getLastMessages(count, withIdMessage);
        List<String> stringMessage = new ArrayList<>();
        for (Message elem : list) {
            stringMessage.add(getStringMessage(elem));
        }
        String gson = new Gson().toJson(stringMessage);
        WriterResponse.sendResponse(response, gson);
    }

    private void giveLastMessages(HttpServletRequest request, HttpServletResponse response) {
        MessageDAO messageDAO = ServletUtils.getMessageDao(request);
        int count = Integer.parseInt(request.getParameter(PARAMETER_LIMIT));
        List<Message> list = messageDAO.getLastMessages(count);
        List<String> stringMessage = new ArrayList<>();
        Collections.reverse(list);
        for (Message elem : list) {
            stringMessage.add(getStringMessage(elem));
        }
        String gson = new Gson().toJson(stringMessage);
        WriterResponse.sendResponse(response, gson);
    }

    private void saveMessage(HttpServletRequest request, HttpServletResponse response) {
        JsonObject jsonObject = ReaderRequests.getRequestData(request);
        UserDAO userDAO = ServletUtils.getUserDao(request);
        MessageDAO messageDAO = ServletUtils.getMessageDao(request);
        int idUser = jsonObject.get(PROPERTY_ID_USER).getAsInt();
        String communication = jsonObject.get(PROPERTY_COMMUNICATION).getAsString().trim();
        if (MessageDataValidator.validateCommunication(communication)) {
            User user = userDAO.getUser(idUser);
            Message message = new Message(user, LocalDateTime.now(), communication, Status.MESSAGE);
            messageDAO.sendMessage(message);
            response.setStatus(STATUS_200);
        } else {
            response.setStatus(STATUS_422);
        }
    }

    private String getStringMessage(Message message) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(PROPERTY_ID_MESSAGE, message.getIdMessage());
        jsonObject.addProperty(PROPERTY_NAME, message.getUser().getName());
        jsonObject.addProperty(PROPERTY_DATE, message.getLocalDateTime().replace("T", " "));
        jsonObject.addProperty(PROPERTY_COMMUNICATION, ParserHtml.parseHtml(message.getCommunication()));
        jsonObject.addProperty(PROPERTY_TITLE_ROLE, message.getUser().getRole().toString());

        return jsonObject.toString();
    }


}
