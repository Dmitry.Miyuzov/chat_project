let showRegistrationForm = function() {
    clearFieldsRegistration();
    clearAllError();
    comeBackDefaultStyleForButton();
    $(idFormLogin).hide();
    $(idFormRegistration).show(1000);
};
let showLoginForm = function(email) {
    $(idLoginEmail).val(email);
    emptyEmailLogin();
    $(idLoginPassword).val(textEmpty);
    emptyPasswordLogin();
    $(idFormRegistration).hide();
    $(idFormLogin).show(1000);
};
$(document).ready(function () {
    $(idFormRegistration).hide();

    $(document.body).on(eventClick,classMenuButtonLogin,function () {
        showLoginForm(textEmpty);
    });

    $(document.body).on(eventClick,classMenuButtonRegistration,function () {
        showRegistrationForm();
    });
});