package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class UserDataValidator {

    private static final String PATTERN_DATA_USER_NAME = "data.user.name.regexp";
    private static final String PATTERN_DATA_USER_EMAIL = "data.user.email.regexp";
    private static final String PATTERN_DATA_USER_PASS = "data.user.password.regexp";

    private UserDataValidator(){

    }

    public static boolean validateEmail(String email) {
        boolean isValidate = false;
        Pattern pattern = Pattern.compile(RegexProvider.getDataUserExpression(PATTERN_DATA_USER_EMAIL));
        Matcher matcher = pattern.matcher(email);
        if (matcher.find()) {
            isValidate = true;
        }
        return isValidate;
    }

    public static boolean validateName (String name) {
        boolean isValidate = false;
        Pattern pattern = Pattern.compile(RegexProvider.getDataUserExpression(PATTERN_DATA_USER_NAME));
        Matcher matcher = pattern.matcher(name);
        if (matcher.find()) {
            isValidate = true;
        }
        return isValidate;
    }

    public static boolean validatePassword (String password) {
        boolean isValidate = false;
        Pattern pattern = Pattern.compile(RegexProvider.getDataUserExpression(PATTERN_DATA_USER_PASS));
        Matcher matcher = pattern.matcher(password);
        if (matcher.find()) {
            isValidate = true;
        }
        return isValidate;
    }

    public static boolean validateCorPassword (String password , String corPassword) {
        boolean isValidate = false;
        if (password.equals(corPassword)) {
            isValidate = true;
        }
        return isValidate;
    }

}
