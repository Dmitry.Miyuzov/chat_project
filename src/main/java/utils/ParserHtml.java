package utils;

public class ParserHtml {

    public static String parseHtml(String text) {
        return text.replace("&", "&amp;").replace("<", "&lt;")
                .replace(">","&gt;").replace("\"", "&quot;")
                .replace("'", "&#039;");
    }

}
