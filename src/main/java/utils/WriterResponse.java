package utils;

import com.google.gson.JsonObject;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public final class WriterResponse {
    private static final Logger LOGGER = Logger.getLogger(WriterResponse.class);
    private static final String CONTENT_TYPE_JSON = "application/json";

    private WriterResponse() {
    }

    public static void sendResponse(HttpServletResponse response, JsonObject jsonData) {
        try (PrintWriter out = response.getWriter()) {
            response.setContentType(CONTENT_TYPE_JSON);
            out.write(String.valueOf(jsonData));
            out.flush();
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    public static void sendResponse(HttpServletResponse response, String data) {
        try (PrintWriter out = response.getWriter()) {
            response.setContentType(CONTENT_TYPE_JSON);
            out.write(data);
            out.flush();
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }
}
