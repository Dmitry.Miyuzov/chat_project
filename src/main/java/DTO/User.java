package DTO;

import java.util.Objects;

public class User {
    private int idUser;
    private String name;
    private String email;
    private String password;
    private Gender gender;
    private Role role;
    private String base64Img;

    public User(int id, String name, String email, Gender gender, Role role, String base64Img) {
        this.idUser = id;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.role = role;
        this.base64Img = base64Img;
    }

    public User(String name, String email, String password, Gender gender, Role role) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public User(String name, String email, String password, Gender gender, Role role, String base64Img) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.role = role;
        this.base64Img = base64Img;
    }

    public String getBase64Img() {
        return base64Img;
    }

    public int getIdUser() {
        return idUser;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return gender == user.gender &&
                Objects.equals(name, user.name) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, password, gender);
    }
}
