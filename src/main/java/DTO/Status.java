package DTO;

public enum Status {
    MESSAGE("The user left a message");

    private String description;

    Status(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
