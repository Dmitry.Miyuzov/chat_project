package DB;

import DB.utils.DBConnection;
import DTO.*;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

final class PostgresSQLRequestHandler {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("queries");
    private static final String EMPTY = "";
    private static final String QUERY_GET_USER = "getUserById";
    private static final String QUERY_GET_ROLE_BY_ID = "getRoleById";
    private static final String QUERY_GET_GENDER_BY_ID = "getGenderById";
    private static final String QUERY_GET_PASS_BY_EMAIL = "getPasswordByEmail";
    private static final String QUERY_GET_EMAIL = "getEmail";
    private static final String QUERY_GET_ID_USER = "getIdUser";
    private static final String QUERY_GET_STATUS_BY_ID = "getStatusById";
    private static final String QUERY_GET_ROLE_BY_TITLE = "getRoleByTitle";
    private static final String QUERY_GET_LAST_MESSAGES = "getLastMessages";
    private static final String QUERY_GET_HISTORY_MESSAGES = "getHistoryMessages";
    private static final String QUERY_GET_ALL_ONLINE_USERS = "getAllOnlineUsers";
    private static final String QUERY_GET_STATUS_BY_TITLE = "getStatusByTitle";
    private static final String QUERY_ADD_USER_TO_DATABASE = "addUserToDataBase";
    private static final String QUERY_ADD_TOKEN_TO_DATABASE = "addTokenToDataBase";
    private static final String QUERY_GET_UPDATE_PASS = "updatePassword";
    private static final String QUERY_SEND_MESSAGE = "saveMessage";
    private static final String COLUMN_ID_USER_FROM_USER = "id_user";
    private static final String COLUMN_ID_ROLE_FROM_USER = "id_role";
    private static final String COLUMN_ID_GENDER_FROM_USER = "id_gender";
    private static final String COLUMN_ID_STATUS_FROM_USER = "id_status";
    private static final String COLUMN_TITLE_GENDER_FROM_GENDER = "title_gender";
    private static final String COLUMN_TITLE_STATUS_FROM_STATUS = "title_status";
    private static final String COLUMN_TITLE_ROLE_FROM_ROLE = "title_role";
    private static final String COLUMN_NAME_FROM_USER = "name";
    private static final String COLUMN_EMAIL_FROM_USER = "email";
    private static final String COLUMN_PASS_FROM_USER = "password";
    private static final String COLUMN_IMAGE_BASE64_FROM_USER = "image_base64";
    private static final String COLUMN_TIMESTAMP_FROM_MESSAGE = "timestamp";
    private static final String COLUMN_ID_MESSAGE_FROM_MESSAGE = "id_message";
    private static final String COLUMN_USER_EMAIL_FROM_MESSAGE = "user_email";
    private static final String COLUMN_MESSAGE_FROM_MESSAGE = "message";
    private static final String COLUMN_STATUS_FROM_MESSAGE = "status";
    private static final String TITLE_MALE = "MALE";
    private static final String TITLE_FEMALE = "FEMALE";
    private static final String TITLE_ADMIN = "ADMIN";
    private static final String TITLE_USER = "USER";
    private static final String TITLE_MESSAGE = "MESSAGE";
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final int ZERO = 0;
    private static final int PARAMETER_INDEX_ONE = 1;
    private static final int PARAMETER_INDEX_TWO = 2;
    private static final int PARAMETER_INDEX_THREE = 3;
    private static final int PARAMETER_INDEX_FOUR = 4;
    private static final int PARAMETER_INDEX_FIVE = 5;
    private static final int PARAMETER_INDEX_SIX = 6;
    private static final int TIME_START_INDEX = 0;
    private static final int TIME_END_INDEX = 19;

    private PostgresSQLRequestHandler() {
    }

    /*
    This method returns list of online users.
     */
    static List<User> getAllOnlineUsers() throws SQLException {
        String query = resourceBundle.getString(QUERY_GET_ALL_ONLINE_USERS);
        List<User> list = new ArrayList<>();
        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int idUser = resultSet.getInt(COLUMN_ID_USER_FROM_USER);
                Role role = getIdRole(resultSet.getInt(COLUMN_ID_ROLE_FROM_USER));
                Gender gender = getGender(resultSet.getInt(COLUMN_ID_GENDER_FROM_USER));
                String name = resultSet.getString(COLUMN_NAME_FROM_USER);
                String email = resultSet.getString(COLUMN_EMAIL_FROM_USER);
                String base64Img = resultSet.getString(COLUMN_IMAGE_BASE64_FROM_USER);
                User user = new User(idUser, name, email, gender, role, base64Img);
                list.add(user);
            }
        }
        return list;
    }

    /*
    This method save message in Database.
     */
    static void saveMessage(Message message) throws SQLException {
        String query = resourceBundle.getString(QUERY_SEND_MESSAGE);
        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(PARAMETER_INDEX_ONE, message.getUser().getEmail());
            statement.setString(PARAMETER_INDEX_TWO, message.getCommunication());
            statement.setInt(PARAMETER_INDEX_THREE, getIdStatus(message.getStatus().toString()));
            statement.executeUpdate();
        }
    }

    /*
    This method returns list of last messages.
    count - sets the number of returned messages
     */
    static List<Message> getLastMessages(int count) throws SQLException {
        String query = resourceBundle.getString(QUERY_GET_LAST_MESSAGES);
        List<Message> list;
        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(PARAMETER_INDEX_ONE, count);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = getLastMessages(resultSet);
            }
        }
        return list;
    }

    /*
    This method returns list of last messages.
    count - sets the number of returned messages
    withIdMessages - what message will the search start from
     */
    static List<Message> getLastMessages(int count, int withIdMessage) throws SQLException {
        String query = resourceBundle.getString(QUERY_GET_HISTORY_MESSAGES);
        List<Message> list;
        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(PARAMETER_INDEX_ONE, withIdMessage);
            statement.setInt(PARAMETER_INDEX_TWO, count);
            try (ResultSet resultSet = statement.executeQuery()) {
                list = getLastMessages(resultSet);
            }
        }
        return list;
    }

    static String getEmail(String email) throws SQLException {
        return getDataOfUser(QUERY_GET_EMAIL, COLUMN_EMAIL_FROM_USER, email);
    }

    static int getIdUser(String email) throws SQLException {
        return getId(QUERY_GET_ID_USER, COLUMN_ID_USER_FROM_USER, email);
    }

    static void addUserToDataBase(User user) throws SQLException {
        String query = resourceBundle.getString(QUERY_ADD_USER_TO_DATABASE);

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(PARAMETER_INDEX_ONE, user.getRole().toString());
            statement.setString(PARAMETER_INDEX_TWO, user.getGender().toString());
            statement.setString(PARAMETER_INDEX_THREE, user.getName());
            statement.setString(PARAMETER_INDEX_FOUR, user.getEmail());
            statement.setString(PARAMETER_INDEX_FIVE, user.getPassword());
            statement.setString(PARAMETER_INDEX_SIX, user.getBase64Img());
            statement.executeUpdate();
        }
    }

    static void addTokenToDataBase(Token token) throws SQLException {
        String query = resourceBundle.getString(QUERY_ADD_TOKEN_TO_DATABASE);

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(PARAMETER_INDEX_ONE, token.getUserId());
            statement.executeUpdate();
        }
    }

    static int getIdRole(String titleRole) throws SQLException {
        return getId(QUERY_GET_ROLE_BY_TITLE, COLUMN_ID_ROLE_FROM_USER, titleRole);
    }

    static User getUser(int idUser) throws SQLException {
        User user = null;
        String query = resourceBundle.getString(QUERY_GET_USER);

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(PARAMETER_INDEX_ONE, idUser);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    String name = resultSet.getString(COLUMN_NAME_FROM_USER);
                    String email = resultSet.getString(COLUMN_EMAIL_FROM_USER);
                    String password = resultSet.getString(COLUMN_PASS_FROM_USER);
                    Gender gender = getGender(resultSet.getInt(COLUMN_ID_GENDER_FROM_USER));
                    Role role = getIdRole(resultSet.getInt(COLUMN_ID_ROLE_FROM_USER));
                    user = new User(name, email, password, gender, role);
                }
            }
        }
        return user;
    }

    static String getPassword(String email) throws SQLException {
        return getDataOfUser(QUERY_GET_PASS_BY_EMAIL, COLUMN_PASS_FROM_USER, email);
    }

    static void updatePassword(int idUser, String pass) throws SQLException {
        String query = resourceBundle.getString(QUERY_GET_UPDATE_PASS);

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(PARAMETER_INDEX_ONE, pass);
            statement.setInt(PARAMETER_INDEX_TWO, idUser);
            statement.executeUpdate();
        }
    }

    private static int getIdStatus(String titleStatus) throws SQLException {
        return getId(QUERY_GET_STATUS_BY_TITLE, COLUMN_ID_STATUS_FROM_USER, titleStatus);
    }

    private static Gender getGender(int idGender) throws SQLException {
        Gender gender = null;
        String query = resourceBundle.getString(QUERY_GET_GENDER_BY_ID);
        String result = EMPTY;

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(PARAMETER_INDEX_ONE, idGender);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result = resultSet.getString(COLUMN_TITLE_GENDER_FROM_GENDER);
                }
            }
            if (result.equals(TITLE_MALE)) {
                gender = Gender.MALE;
            } else if (result.equals(TITLE_FEMALE)) {
                gender = Gender.FEMALE;
            }
        }
        return gender;


    }

    private static Role getIdRole(int idRole) throws SQLException {
        Role role = null;
        String query = resourceBundle.getString(QUERY_GET_ROLE_BY_ID);
        String result = EMPTY;

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(PARAMETER_INDEX_ONE, idRole);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result = resultSet.getString(COLUMN_TITLE_ROLE_FROM_ROLE);
                }
            }
            if (result.equals(TITLE_ADMIN)) {
                role = Role.ADMIN;
            } else if (result.equals(TITLE_USER)) {
                role = Role.USER;
            }
        }
        return role;
    }

    private static Status getStatus(int idStatus) throws SQLException {
        Status status = null;
        String query = resourceBundle.getString(QUERY_GET_STATUS_BY_ID);
        String result = EMPTY;

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(PARAMETER_INDEX_ONE, idStatus);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result = resultSet.getString(COLUMN_TITLE_STATUS_FROM_STATUS);
                }
            }
            if (result.equals(TITLE_MESSAGE)) {
                status = Status.MESSAGE;
            }
        }
        return status;
    }

    private static LocalDateTime getTime(ResultSet resultSet) throws SQLException {
        String timeWithMilliseconds = resultSet.getString(COLUMN_TIMESTAMP_FROM_MESSAGE);
        String timeWithoutMilliseconds = timeWithMilliseconds.substring(TIME_START_INDEX, TIME_END_INDEX);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.ENGLISH);
        return LocalDateTime.parse(timeWithoutMilliseconds, formatter);
    }

    private static List<Message> getLastMessages(ResultSet resultSet) throws SQLException {
        List<Message> list = new ArrayList<>();
        while (resultSet.next()) {
            int idMessage = resultSet.getInt(COLUMN_ID_MESSAGE_FROM_MESSAGE);
            User user = getUser(getIdUser(resultSet.getString(COLUMN_USER_EMAIL_FROM_MESSAGE)));
            String communication = resultSet.getString(COLUMN_MESSAGE_FROM_MESSAGE);
            LocalDateTime localDateTime = getTime(resultSet);
            Status status = getStatus(resultSet.getInt(COLUMN_STATUS_FROM_MESSAGE));
            Message message = new Message(user, localDateTime, communication, status);
            message.setIdMessage(idMessage);
            list.add(message);
        }
        return list;
    }

    private static String getQuery(String nameQuery) {
        return resourceBundle.getString(nameQuery);
    }

    private static int getId(String nameQuery, String columnName, String famousData) throws SQLException {
        String query = getQuery(nameQuery);
        int result = ZERO;

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(PARAMETER_INDEX_ONE, famousData);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result = resultSet.getInt(columnName);
                }
            }
        }
        return result;
    }

    private static String getDataOfUser(String nameQuery, String columnName, String famousData) throws SQLException {
        String query = getQuery(nameQuery);
        String result = EMPTY;

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(PARAMETER_INDEX_ONE, famousData);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result = resultSet.getString(columnName);
                }
            }
        }
        return result;
    }

}
