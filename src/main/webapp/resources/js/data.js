const accessToken = "accessToken";
const refreshToken = "refreshToken";
const textNull = "null";
const eventKeyUp = "keyup";
const eventClick = "click";
const textSlow = "slow";
const textDisabled = "disabled";
const textBorder = "border";
const textColor = "color";
const textBody = "body";
const textSrc = "src";
const textBackgroundColor = "background-color";
const textEmpty = "";
const colorGreen = "#336600";
const colorRed = "#ff0000";
const patternForName = "^[A-Za-zА-Яа-яЁё][A-Za-zа-яА-ЯЁё]{2,20}$";
const patternForEmail = "^[A-Za-z0-9]+@[A-Z-a-z]{1,6}\\.[A-Za-z]{2,5}$";
const patternForPassword = "^[A-Za-z0-9]{4,14}[A-Za-z0-9]$";
const patternForMessage = "^[A-Za-zА-Яа-яЁё0-9\\s\\W][A-Za-zа-яА-ЯЁё0-9\\s\\W]{0,249}$";
const divMainContainer = "div.main-container";
const classMenuLoginRegistration = ".menu-login-registration";
const classMenuButtonLogin = ".menu-button-login";
const classLoginErrorEmail = ".login-error-email";
const classLoginErrorPassword = ".login-error-password";
const classMenuButtonRegistration = ".menu-button-registration";
const classRegistrationErrorName = ".registration-error-name";
const classRegistrationErrorEmail = ".registration-error-email";
const classRegistrationErrorPassword = ".registration-error-password";
const classRegistrationErrorCorPassword = ".registration-error-cor-password";
const classButtonLogoutUser = ".button-logout-user";
const classButtonHomeUser = ".button-home-user";
const classButtonHomeAdmin = ".button-home-admin";
const classButtonLogoutAdmin = ".button-logout-admin";
const classNewPasswordError = ".new-password-error";
const classNewCorPasswordError = ".new-cor-password-error";
const classOldPasswordError = ".old-password-error";
const classUserListLi = ".user-list-li";
const classChatInputMessage = ".chat-input-message";
const classUserInfo = ".user-info";
const classUserInfoCurrent = ".user-info-current";
const classChat = ".chat";
const classChatWindow = ".chat-window";
const classLiMessage = ".li-message";
const idFormRegistration = "#form-registration";
const idFormLogin = "#form-login";
const idLoginEmail = "#login-email";
const idLoginPassword = "#login-password";
const idLoginSubmit = "#login-submit";
const idRegistrationSubmit = "#registration-submit";
const idRegistrationName = "#registration-name";
const idRegistrationEmail = "#registration-email";
const idRegistrationPassword = "#registration-password";
const idRegistrationCorPassword = "#registration-cor-password";
const idRegistrationIconPassword = "#registration-icon-password";
const idRegistrationIconCorPassword = "#registration-icon-cor-password";
const idLoginIconPassword = "#login-icon-password";
const idOldPassword = "#old-password";
const idNewPassword = "#new-password";
const idNewCorPassword = "#new-cor-password";
const idOldPasswordIcon = "#old-password-icon";
const idNewPasswordIcon = "#new-password-icon";
const idNewCorPasswordIcon = "#new-cor-password-icon";
const idChangeUserSubmit = "#change-user-submit";
const idChangePassword = "#change-password";
const idRegistrationGender = "#registration-gender";
const idRegistrationImage = "#registration-image";
const idButtonLoadMessages = "#button-load-messages";
const idFile = "#file";
const idMyList = "#myList";
const controlIconPassword = function (eventElement, changeElement) {
    $(document.body).on("click", eventElement, function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        let a = $(eventElement).hasClass("fa fa-eye");
        if (a) {
            $(changeElement).prop("type", "text");
        } else {
            $(changeElement).prop("type", "password");
        }
    });
};

const logout = function () {
    $.ajax({
        url: "/logout",
        headers: {
            accessToken : localStorage.getItem(accessToken),
            refreshToken : localStorage.getItem(refreshToken)
        },
        cache: false,
        method: "PATCH",

        statusCode: {
            200: function () {
                showLogout();
            }
        }
    });
};
const showLogout = function() {
    document.body.innerHTML = textEmpty;
    localStorage.setItem(accessToken, textNull);
    localStorage.setItem(refreshToken, textNull);
    $(menuLoginRegistration).appendTo(textBody);
    $(formRegistration).appendTo(textBody);
    $(idFormRegistration).hide();
    $(formLogin).appendTo(textBody);
};
const viewRegistrationWhenSessionInvalidated = function() {
    showLogout();
    Swal.fire({
        icon: "info",
        title: 'Вы зашли с другого устройства!',
        confirmButtonText: "Ок",
        confirmButtonColor: "#6666ff",
        allowEscapeKey: false,
        position: 'top',
    })
};
const getIdUser = function(accessTokenString) {
    let partsOfToken = accessTokenString.split(".");
    let payLoad = atob(partsOfToken[1]);
    let json = JSON.parse(payLoad);
    return json.id_user;
};
const isAdmin = function (accessTokenString) {
    let partsOfToken = accessTokenString.split(".");
    let payLoad = atob(partsOfToken[1]);
    let json = JSON.parse(payLoad);
    let idRole = json.id_role;
    let result;
    result = idRole === 1;
    return result;
};
const viewAfterLogin = function(accessTokenString) {
    let result = isAdmin(accessTokenString);
    if (result) {
        document.body.innerHTML = textEmpty;
        $(chatAdmin).appendTo(textBody);
        $(chatGeneral).appendTo(textBody);
    } else {
        document.body.innerHTML = textEmpty;
        $(chatUser).appendTo(textBody);
        $(chatGeneral).appendTo(textBody);
    }
    showButtonLoadMessage();
};
const eventHome = function(eventElement) {
    $(document.body).on(eventClick, eventElement, function () {
        authenticationWithTwoStatus(viewAfterLogin, viewRegistrationWhenSessionInvalidated);
    });
};


/*
The variable fields of the registration
*/
let isReadyName = false;
let isReadyEmail = false;
let isReadyPassword = false;
let isReadyCorPassword = false;

/*
The variable fields of the login
*/
let isReadyLoginEmail = false;
let isReadyLoginPassword = false;
/*
The variable fields for changes password.
 */
let isReadyNewPassword = false;
let isReadyNewCorPassword = false;

