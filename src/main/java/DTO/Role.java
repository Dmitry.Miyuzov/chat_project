package DTO;

public enum Role {
    ADMIN("This is role - ADMIN"),
    USER("This is role - USER");

    private String description;
    private static final String ADMIN_EMAIL_LAST_CHARACTER = "@epam.com";

    Role(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static Role getRoleByEmail(String email) {
        Role role;
        if (email.endsWith(ADMIN_EMAIL_LAST_CHARACTER)) {
            role = Role.ADMIN;
        } else {
            role = Role.USER;
        }
        return role;
    }
}
