let lastIdMessage = 0;
let createDivUserName = function(name){
    let userName = document.createElement("div");
    userName.setAttribute("class", "user-data-name");
    userName.innerText = "Имя: " + name;
    return userName;
};
let createDivUserGender = function(titleGender) {
    let userGender = document.createElement("div");
    userGender.setAttribute("class", "user-data-gender");
    if (titleGender === "MALE") {
        userGender.innerText = "Пол: Мужской";
    } else if (titleGender === "FEMALE") {
        userGender.innerText = "Пол: Женский";
    }
    return userGender;
};

let createDivUserRole = function(titleRole) {
    let userRole = document.createElement("div");
    userRole.setAttribute("class", "user-data-role");
    if (titleRole === "USER") {
        userRole.innerText = "Роль: Пользователь";
    } else if (titleRole === "ADMIN") {
        userRole.innerText = "Роль: Администатор";
    }
    return userRole;
};
let createDivUserPhoto = function(base64Img) {
    let userPhoto = document.createElement("div");
    userPhoto.setAttribute("class","user-photo");
    let img = document.createElement("img");
    img.setAttribute("class", "image-info");
    img.setAttribute("src", base64Img);
    userPhoto.appendChild(img);
    return userPhoto;
};

let createDivUserData = function(name, titleGender, titleRole) {
    let userData = document.createElement("div");
    userData.setAttribute("class","user-data");
    let divUserName = createDivUserName(name);
    let divUserGender = createDivUserGender(titleGender);
    let divUserRole = createDivUserRole(titleRole);
    userData.appendChild(divUserName);
    userData.appendChild(divUserGender);
    userData.appendChild(divUserRole);
    return userData;
};
let createDivUserInfo = function(name,email,titleGender,titleRole, base64Img) {
    let userInfo = document.createElement("div");
    userInfo.setAttribute("class", "user-info");
    let divUserPhoto = createDivUserPhoto(base64Img);
    let divUserData = createDivUserData(name,titleGender,titleRole);
    userInfo.appendChild(divUserPhoto);
    userInfo.appendChild(divUserData);
    return userInfo;
};

let getLastIdMessage = function(numberIteration) {
    if (numberIteration === 0) {
        let child = document.querySelector(".li-message").childNodes;
        let firstChild = child[0];
        lastIdMessage = firstChild.getAttribute("id_message");
    }
};
let getLastIdMessageAfterLoadHistory = function() {
    let child = document.querySelector(".li-message").childNodes;
    let firstChild = child[0];
    lastIdMessage = firstChild.getAttribute("id_message");
};
let showButtonLoadMessage = function() {
    $(idButtonLoadMessages).hide();
    if (lastIdMessage > 1) {
        $(idButtonLoadMessages).removeClass("empty-history-messages");
        $(idButtonLoadMessages).addClass("loadMessages");
        $(idButtonLoadMessages).val("Ещё");
        $(idButtonLoadMessages).show();
    } else {
        $(idButtonLoadMessages).removeClass("loadMessages");
        $(idButtonLoadMessages).addClass("empty-history-messages");
        $(idButtonLoadMessages).val("Сообщений больше нет");
        $(idButtonLoadMessages).show();
    }
};
let createLiElementForChatUsers = function(idUser, name,titleGender,email, titleRole, base64Img) {
    let li = document.createElement("li");
    let span = document.createElement("span");
    let divUserInfo = createDivUserInfo(name,email,titleGender,titleRole, base64Img);
    if (titleRole === "ADMIN") {
        li.setAttribute("id_user", idUser);
        span.setAttribute("data-id-user-cor", idUser);
        span.setAttribute("class", "admin");
        span.innerHTML = name;
        if (idUser === getIdUser(localStorage.getItem(accessToken))) {
            li.append("Вы: ");
        }
        li.appendChild(span);
        li.appendChild(divUserInfo);
        $(classUserListLi).append(li);
    } else if (titleRole === "USER") {
        li.setAttribute("id_user", idUser);
        span.setAttribute("data-id-user-cor", idUser);
        span.setAttribute("class", "user");
        span.innerHTML = name;
        if (idUser === getIdUser(localStorage.getItem(accessToken))) {
            li.append("Вы: ");
        }
        li.appendChild(span);
        li.appendChild(divUserInfo);
        $(classUserListLi).append(li);
    }
};
let createLiElementForChatMessages = function(idMessage, name, date, communication, titleRole) {
    let li = document.createElement("li");
  if (titleRole === "ADMIN") {
      li.setAttribute("id_message", idMessage);
      li.innerHTML = "<span class='admin'>" + date + " " + name + ": " + "</span>"  + communication;
      $(classLiMessage).append(li);
  } else if (titleRole === "USER") {
      li.setAttribute("id_message", idMessage);
      li.innerHTML = "<span class='user'>" + date + " " + name + ": " + "</span>"  + communication;
      $(classLiMessage).append(li)
  }
};
let createHistoryLiElementForChatMessages = function (idMessage, name, date, communication, titleRole) {
    let li = document.createElement("li");
    if (titleRole === "ADMIN") {
        li.setAttribute("id_message", idMessage);
        li.innerHTML = "<span class='admin'>" + date + " " + name + ": " + "</span>"  + communication;
        $(classLiMessage).prepend(li)
    } else if (titleRole === "USER") {
        li.setAttribute("id_message", idMessage);
        li.innerHTML = "<span class='user'>" + date + " " + name + ": " + "</span>"  + communication;
        $(classLiMessage).prepend(li)
    }
};
let refreshOnlineUsers = function(dataFromServer) {
    let idListNewOnlineUser = [];
    let listCurrentOnlineUser = document.querySelector(".user-list-li").childNodes;
    let idListCurrentOnlineUser = [];
    for (let i = 0; i < dataFromServer.length; i++) {
        let obj = JSON.parse(dataFromServer[i]);
        let idUser = obj.idUser;
        let name = obj.name;
        let email = obj.email;
        let titleGender = obj.titleGender;
        let titleRole = obj.titleRole;
        let image = obj.base64Img;
        let selector = "[id_user=" + "'" + idUser + "']";
        if (document.querySelector(selector) == null) {
            createLiElementForChatUsers(idUser,name,titleGender,email,titleRole, image);
        }
        idListNewOnlineUser.push(idUser);
    }

    for (let i = 0; i < listCurrentOnlineUser.length; i++) {
        idListCurrentOnlineUser.push(Number(listCurrentOnlineUser[i].getAttribute("id_user")));
    }
    for (let i = 0; i < idListCurrentOnlineUser.length; i++){
        if (idListNewOnlineUser.indexOf(idListCurrentOnlineUser[i]) === -1) {
            document.querySelector("[id_user=" + "'" + idListCurrentOnlineUser[i] + "']").remove();
        }
    }
};
let createMessages = function(dataFromServer) {
  for (let i = 0; i < dataFromServer.length; i++) {
      let obj = JSON.parse(dataFromServer[i]);
      let idMessage = obj.idMessage;
      let name = obj.name;
      let date = obj.date;
      let communication = obj.communication;
      let titleRole = obj.titleRole;
      let selector = "[id_message=" + "'" + idMessage + "']";
      if (document.querySelector(selector) == null) {
          createLiElementForChatMessages(idMessage, name, date, communication , titleRole);
          $(idMyList).animate({scrollTop: $(idMyList).prop("scrollHeight")}, 500);
          getLastIdMessage(i);
          showButtonLoadMessage();
      }
  }

};
let createHistoryMessages = function(dataFromServer) {
    for (let i = 0; i < dataFromServer.length; i++) {
        let obj = JSON.parse(dataFromServer[i]);
        let idMessage = obj.idMessage;
        let name = obj.name;
        let date = obj.date;
        let communication = obj.communication;
        let titleRole = obj.titleRole;
        let selector = "[id_message=" + "'" + idMessage + "']";
        if (document.querySelector(selector) == null) {
            createHistoryLiElementForChatMessages(idMessage, name, date, communication , titleRole);
            if (i === dataFromServer.length - 1) {
                getLastIdMessageAfterLoadHistory();
            }
            showButtonLoadMessage();
        }
    }
};
let getAllOnlineUsers = function() {
    $.ajax({
        url : "/users",
        method: "GET",
        cache: false,
        contentType: "application/json ; charset=utf-8",
        dataType: "json",

        statusCode: {
            200: function (data) {
                refreshOnlineUsers(data);
            }
        }

    })
};
let getLastMessages = function() {
    $.ajax({
        url : "/messages?limit=10",
        method: "GET",
        cache: false,
        contentType: "application/json ; charset=utf-8",
        dataType: "json",

        success: function (data) {
        createMessages(data);
        }

    })
};
let getHistoryMessages = function() {
    $.ajax({
        url : "/messages?limit=10&lastMessage=" + lastIdMessage,
        method: "GET",
        cache: false,
        contentType: "application/json ; charset=utf-8",
        dataType: "json",

        success: function (data) {
            createHistoryMessages(data)
        }
    })
};
let errorIncorrectMessage = function() {
    Swal.fire({
        icon: "error",
        title: 'Сообщение не должно быть пустым и содержать не более 250 символов',
        confirmButtonText: "Ок",
        confirmButtonColor: "#6666ff",
        allowEscapeKey: false,
        position: 'top',
    });
};
let sendMessage = function() {
    let pattern = new RegExp(patternForMessage);
    let message = JSON.stringify({
        'idUser' : getIdUser(localStorage.getItem(accessToken)),
        'communication' : $(classChatInputMessage).val().trim(),
    });
    if (pattern.test($(classChatInputMessage).val().trim())) {
        $(classChatInputMessage).val("");
        $.ajax({
            url : "/messages",
            method: "POST",
            cache: false,
            contentType: "application/json ; charset=utf-8",
            dataType: "json",
            data : message,

            statusCode: {
                200: function () {
                    getLastMessages();
                },
                422: function () {
                    errorIncorrectMessage();
                }
            }
        });
    } else {
        errorIncorrectMessage();
    }

};

$(document).ready(function () {

    $(document).on({

        mouseenter: function () {
            let parent = $(this).parent();
            let child = $(parent).find(classUserInfo);
            $(child).removeClass("user-info");
            $(child).addClass("user-info-current");
            $(classChatWindow).css("filter", "blur(2px)");
            $(classChat).css("filter", "blur(2px)")
        },
        mouseleave: function () {
            let parent = $(this).parent();
            let child = $(parent).find(classUserInfoCurrent);
            $(child).removeClass("user-info-current");
            $(child).addClass("user-info");
            $(classChatWindow).css("filter", "blur(0px)");
            $(classChat).css("filter", "blur(0px)")
        }
    }, "[data-id-user-cor]");



    setInterval(function () {
        if (document.querySelector(".main-container")) {
            authenticationWithTwoStatus(getAllOnlineUsers, viewRegistrationWhenSessionInvalidated);
        }
    }, 2000);

    setInterval(function () {
        if (document.querySelector(".main-container")) {
            authenticationWithTwoStatus(getLastMessages, viewRegistrationWhenSessionInvalidated);
        }
    }, 2000);

    $(document.body).on("click", ".loadMessages", function () {
        authenticationWithTwoStatus(getHistoryMessages, viewRegistrationWhenSessionInvalidated);
    });

    $(document.body).on("click", ".chat-input-button", function() {
        authenticationWithTwoStatus(sendMessage, viewRegistrationWhenSessionInvalidated);
        return false;
    });

    $(document.body).on("submit", "#form-message", function() {
        authenticationWithTwoStatus(sendMessage, viewRegistrationWhenSessionInvalidated);
        return false;
    });
});