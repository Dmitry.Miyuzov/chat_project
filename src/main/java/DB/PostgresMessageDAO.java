package DB;

import DAO.MessageDAO;
import DTO.Message;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

public class PostgresMessageDAO implements MessageDAO {
    private static final Logger LOGGER = Logger.getLogger(PostgresMessageDAO.class);

    @Override
    public void sendMessage(Message message) {
        try {
            PostgresSQLRequestHandler.saveMessage(message);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public List<Message> getLastMessages(int count) {
        List<Message> result = null;
        try {
            result = PostgresSQLRequestHandler.getLastMessages(count);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public List<Message> getLastMessages(int count, int withIdMessage) {
        List<Message> result = null;
        try {
            result = PostgresSQLRequestHandler.getLastMessages(count,withIdMessage);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }
}
