

/*
Check all fields of the login and changing the button property to available or not
*/
let changePropertiesOfLoginButton = function() {
    if (isReadyLoginEmail && isReadyLoginPassword) {
        $(idLoginSubmit).removeAttr(textDisabled);
    } else {
        $(idLoginSubmit).attr(textDisabled, textDisabled);
    }
};

/*
These functions change style of the email field and it value, also it changes button property available or not.
 */
let trueEmailLogin = function() {
    $(idLoginEmail).css(textBorder, "2px solid #336600");
    $(classLoginErrorEmail).css(textColor, colorGreen);
    $(classLoginErrorEmail).html("Адрес почты корректный");
    isReadyLoginEmail = true;
    changePropertiesOfLoginButton();
};
let errorIncorrectEmailLogin = function() {
    $(idLoginEmail).css(textBorder, "2px solid #e34761");
    $(classLoginErrorEmail).css(textColor, colorRed);
    $(classLoginErrorEmail).html("Адрес почты не корректен");
    isReadyLoginEmail = false;
    changePropertiesOfLoginButton();
};
let emptyEmailLogin = function() {
    $(idLoginEmail).css(textBorder, textEmpty);
    $(classLoginErrorEmail).html(textEmpty);
    isReadyLoginEmail = false;
    changePropertiesOfLoginButton();
};
/*
These functions change style of the password field and it value, also it changes button property available or not.
 */
let truePasswordLogin = function() {
    $(idLoginPassword).css(textBorder, "2px solid #336600");
    $(classLoginErrorPassword).css(textColor, colorGreen);
    $(classLoginErrorPassword).html("Пароль корректен");
    isReadyLoginPassword = true;
    changePropertiesOfLoginButton();
};
let errorIncorrectPasswordLogin = function() {
    $(idLoginPassword).css(textBorder, "2px solid #e34761");
    $(classLoginErrorPassword).css(textColor, colorRed);
    $(classLoginErrorPassword).html("Пароль должен содержать латинские буквы и цифры. От 5 до 16 символов.");
    isReadyLoginPassword = false;
};
let emptyPasswordLogin = function() {
    $(idLoginPassword).css(textBorder, textEmpty);
    $(classLoginErrorPassword).html(textEmpty);
    isReadyLoginPassword = false;
    changePropertiesOfLoginButton();
};

let clearFieldsLogin = function() {
    $(idLoginPassword).val(textEmpty);
    $(idLoginEmail).val(textEmpty);
    emptyEmailLogin();
    emptyPasswordLogin();
};

$(document).ready(function () {

    /*
    When we hover the cursor on button of login
    */
    $(document).on({
        mouseenter: function () {
            $(classMenuButtonLogin).css(textBackgroundColor, '#0e5699')
        },
        mouseleave: function () {
            $(classMenuButtonLogin).css(textBackgroundColor, '#77b3d6')
        }
    }, classMenuButtonLogin);

    /*
    Validation of the field email
    */
    $(document.body).on(eventKeyUp,idLoginEmail, function () {

        let pattern = new RegExp(patternForEmail);
        let emailValue = $(this).val();

        if (emailValue !== textEmpty) {
            if (pattern.test(emailValue)) {
                trueEmailLogin();
            } else {
                errorIncorrectEmailLogin();
            }
        } else {
            emptyEmailLogin()
        }
    });

    /*
    Validation of the field password
    */
    $(document.body).on(eventKeyUp,idLoginPassword, function () {
        let pattern = new RegExp(patternForPassword);
        let passwordValue = $(this).val();

        if (passwordValue !== textEmpty) {
            if (pattern.test(passwordValue)) {
                truePasswordLogin();
            } else {
                errorIncorrectPasswordLogin();
            }
        } else {
            emptyPasswordLogin();
        }
    });

    /*
    Control hide/show password
    */
    controlIconPassword(idLoginIconPassword, idLoginPassword );

    /*
    Send form login
     */
    $(document.body).on(eventClick, idLoginSubmit, function () {
        let formLogin = JSON.stringify({
            'email': $(idLoginEmail).val().trim(),
            'password': $(idLoginPassword).val().trim(),
        });
        $.ajax({
            url: "/login",
            method: "post",
            cache: false,
            contentType: "application/json ; charset=utf-8",
            data: formLogin,
            dataType: "json",

            statusCode: {
                200: function (jsonFromServer) {
                    localStorage.setItem(accessToken, jsonFromServer.accessToken);
                    localStorage.setItem(refreshToken, jsonFromServer.refreshToken);
                    clearFieldsLogin();
                    viewAfterLogin(jsonFromServer.accessToken);
                },
                422: function () {
                    Swal.fire({
                        icon: "error",
                        title: 'Неправильный Email или пароль',
                        confirmButtonText: "Ок",
                        confirmButtonColor: "#6666ff",
                        allowEscapeKey: false,
                        position: 'top',
                    });
                }
            }
        });
    });
});