package DB;

import DAO.TokenDAO;
import DAO.UserDAO;
import DB.utils.DBConnection;
import DTO.User;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.apache.log4j.Logger;
import utils.ServletUtils;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Base64;
import java.util.ResourceBundle;

public class PostgresTokenDAO implements TokenDAO {
    public static final String ACCESS_TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String ID_USER_KEY = "id_user";
    private static final Logger LOGGER = Logger.getLogger(PostgresTokenDAO.class);
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("queries");
    private static final String QUERY_UPDATE_ACCESS_TOKEN = "updateAccessToken";
    private static final String QUERY_UPDATE_REFRESH_TOKEN = "updateRefreshToken";
    private static final String QUERY_GET_ACCESS_TOKEN = "getAccessTokenOfUser";
    private static final String QUERY_GET_REFRESH_TOKEN = "getRefreshTokenOfUser";
    private static final String QUERY_DIED_TIME_ACCESS_TOKEN = "getDiedTimeAccessToken";
    private static final String QUERY_DIED_TIME_REFRESH_TOKEN = "getDiedTimeRefreshToken";
    private static final String QUERY_RESET_TOKEN = "resetToken";
    private static final String COLUMN_DIED_TIME_TOKEN = "died_time_token";
    private static final String ACCESS_SECRET_USER = "Access.-.Secret.-.Key.-.For.-.User.-.With.-.Role.-.User";
    private static final String ACCESS_SECRET_ADMIN = "Access.-.Secret.-.Key.-.For.-.User.-.With.-.Role.-.Admin";
    private static final String REFRESH_SECRET_USER = "Refresh.-.Secret.-.Key.-.For.-.User.-.With.-.Role.-.User";
    private static final String REFRESH_SECRET_ADMIN = "Refresh.-.Secret.-.Key.-.For.-.User.-.With.-.Role.-.Admin";
    private static final String TYPE = "typ";
    private static final String ALG = "alg";
    private static final String TYPE_TOKEN_JWT = "JWT";
    private static final String ALG_HS384 = "HS384";
    private static final String ID_ROLE_KEY = "id_role";
    private static final String IAT = "iat";
    private static final String EXP = "exp";
    private static final String USER_ROLE = "USER";
    private static final long TEN_MINUTES_IN_SECONDS = 600;
    private static final long SEVEN_DAYS_IN_SECONDS = 604800;
    private static final int ZERO = 0;
    private static final int PARAMETER_INDEX_ONE = 1;
    private static final int PARAMETER_INDEX_TWO = 2;
    private static final int PARAMETER_INDEX_THREE = 3;
    private static final int PARAMETER_INDEX_FOUR = 4;

    @Override
    public String createAccessToken(HttpServletRequest request, User user) {
        String accessSecret = getAccessSecretKey(user);
        SecretKey key = Keys.hmacShaKeyFor(accessSecret.getBytes(StandardCharsets.UTF_8));

        return Jwts.builder()
                .setHeaderParam(ALG, ALG_HS384)
                .setHeaderParam(TYPE, TYPE_TOKEN_JWT)
                .setClaims(createClaims(request, user, TEN_MINUTES_IN_SECONDS))
                .signWith(key)
                .compact();
    }

    @Override
    public String createRefreshToken(HttpServletRequest request, User user) {
        String refreshSecret = getRefreshSecretKey(user);

        SecretKey key = Keys.hmacShaKeyFor(refreshSecret.getBytes(StandardCharsets.UTF_8));

        return Jwts.builder()
                .setHeaderParam(ALG, ALG_HS384)
                .setHeaderParam(TYPE, TYPE_TOKEN_JWT)
                .setClaims(createClaims(request, user, SEVEN_DAYS_IN_SECONDS))
                .signWith(key)
                .compact();
    }

    @Override
    public void updateAccessTokenToDataBase(String accessToken) {
        updateTokenToDataBase(accessToken, QUERY_UPDATE_ACCESS_TOKEN);
    }

    @Override
    public void updateRefreshTokenToDataBase(String refreshToken) {
        updateTokenToDataBase(refreshToken, QUERY_UPDATE_REFRESH_TOKEN);
    }

    @Override
    /*
    This method returns access Token or refresh Token.
    1.To select access token - nameToken must be equal to PostgresTokenDAO.ACCESS_TOKEN
    2.To select refresh token - nameToken must be equal to PostgresTokenDAO.REFRESH_TOKEN
    3. if you made a mistake - returns AccessToken.
     */
    public String getTokenOfUser(HttpServletRequest request, User user, String nameToken) {
        String query = RESOURCE_BUNDLE.getString(QUERY_GET_ACCESS_TOKEN);
        String column = ACCESS_TOKEN;
        if (nameToken.equals(REFRESH_TOKEN)) {
            query = RESOURCE_BUNDLE.getString(QUERY_GET_REFRESH_TOKEN);
            column = REFRESH_TOKEN;
        }
        UserDAO userDAO = ServletUtils.getUserDao(request);
        String result = "";

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(PARAMETER_INDEX_ONE, userDAO.getIdUser(user.getEmail()));
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result = resultSet.getString(column);
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public void resetToken(String token) {
        String query = RESOURCE_BUNDLE.getString(QUERY_RESET_TOKEN);
        Claims claims = getClaimsFromToken(token);
        try (Connection connection = DBConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(PARAMETER_INDEX_ONE, (int) claims.get(PostgresTokenDAO.ID_USER_KEY));
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
        }

    }

    /*
        This method returns died time toke.
        1. To select access token - nameToken must be equal to PostgresTokenDAO.ACCESS_TOKEN
        2. To select refresh token - nameToken must be equal to PostgresTokenDAO.REFRESH_TOKEN
        3. if you'll paste other row - return 0
         */
    public long getDiedTimeToken(HttpServletRequest request, User user, String nameToken) {
        UserDAO userDAO = ServletUtils.getUserDao(request);
        long result = ZERO;
        String query;
        if (nameToken.equals(REFRESH_TOKEN) || nameToken.equals(ACCESS_TOKEN)) {
            if (nameToken.equals(ACCESS_TOKEN)) {
                query = RESOURCE_BUNDLE.getString(QUERY_DIED_TIME_ACCESS_TOKEN);
            } else {
                query = RESOURCE_BUNDLE.getString(QUERY_DIED_TIME_REFRESH_TOKEN);
            }
            try (Connection connection = DBConnection.getConnection();
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setInt(PARAMETER_INDEX_ONE, userDAO.getIdUser(user.getEmail()));
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        result = resultSet.getLong(COLUMN_DIED_TIME_TOKEN);
                    }
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return result;
    }

    public boolean isAliveToken(long diedTime) {
        boolean isAlive = false;
        long nowTime = Instant.now().getEpochSecond();
        if (nowTime < diedTime) {
            isAlive = true;
        }
        return isAlive;
    }

    public Claims getClaimsFromToken(String token) {
        String[] partsOfToken = token.split("\\.");
        Claims claims = null;
        try {
            if (partsOfToken.length == PARAMETER_INDEX_THREE) {
                byte[] bytes = Base64.getUrlDecoder().decode(partsOfToken[PARAMETER_INDEX_ONE]);
                String payLoad = new String(bytes, StandardCharsets.UTF_8);
                JsonParser jsonParser = new JsonParser();
                JsonObject jsonObject = jsonParser.parse(payLoad).getAsJsonObject();

                claims = Jwts.claims();
                claims.put(ID_USER_KEY, jsonObject.getAsJsonPrimitive(ID_USER_KEY).getAsInt());
                claims.put(ID_ROLE_KEY, jsonObject.getAsJsonPrimitive(ID_ROLE_KEY).getAsLong());
                claims.put(IAT, jsonObject.getAsJsonPrimitive(IAT).getAsLong());
                claims.put(EXP, jsonObject.getAsJsonPrimitive(EXP).getAsLong());
            }
        } catch (IllegalStateException e) {
            LOGGER.info("Attempt to pick up a token");
        }


        return claims;
    }


    private void updateTokenToDataBase(String token, String queryName) {
        Claims claims = getClaimsFromToken(token);
        if (claims != null) {
            long createTimeToken = (long) claims.get(IAT);
            long diedTimeToken = (long) claims.get(EXP);
            String query = RESOURCE_BUNDLE.getString(queryName);
            try (Connection connection = DBConnection.getConnection();
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(PARAMETER_INDEX_ONE, token);
                statement.setLong(PARAMETER_INDEX_TWO, createTimeToken);
                statement.setLong(PARAMETER_INDEX_THREE, diedTimeToken);
                statement.setInt(PARAMETER_INDEX_FOUR, (Integer) claims.get(ID_USER_KEY));
                statement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        } else {
            LOGGER.info("Attempt to pick up a token");
        }
    }

    private Claims createClaims(HttpServletRequest request, User user, long lifeTimeToken) {
        UserDAO userDAO = ServletUtils.getUserDao(request);
        Claims claims = Jwts.claims();
        claims.put(ID_USER_KEY, userDAO.getIdUser(user.getEmail()));
        claims.put(ID_ROLE_KEY, userDAO.getRole(user.getRole().toString()));
        claims.put(IAT, Instant.now().getEpochSecond());
        claims.put(EXP, Instant.now().getEpochSecond() + lifeTimeToken);
        return claims;
    }

    private String getAccessSecretKey(User user) {
        return user.getRole().toString().equals(USER_ROLE)
                ? ACCESS_SECRET_USER : ACCESS_SECRET_ADMIN;
    }

    private String getRefreshSecretKey(User user) {
        return user.getRole().toString().equals(USER_ROLE)
                ? REFRESH_SECRET_USER : REFRESH_SECRET_ADMIN;
    }
}
