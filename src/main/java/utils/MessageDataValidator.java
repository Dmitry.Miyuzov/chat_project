package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class MessageDataValidator {

    private static final String PATTERN_DATA_MESSAGE_COMMUNICATION = "data.message.communication.regexp";

    private MessageDataValidator() {
    }

    public static boolean validateCommunication (String communication) {
        boolean isValidate = false;
        Pattern pattern = Pattern.compile(RegexProvider.getDataMessageExpressions(PATTERN_DATA_MESSAGE_COMMUNICATION));
        Matcher matcher = pattern.matcher(communication);
        if (matcher.find()) {
            isValidate = true;
        }
        return isValidate;
    }
}
