package serviсes;

import DAO.TokenDAO;
import DB.PostgresTokenDAO;
import io.jsonwebtoken.Claims;
import utils.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutService implements Service {
    private static final String HEADER_ACCESS_TOKEN = "accessToken";


    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        TokenDAO tokenDAO = ServletUtils.getTokenDao(req);
        tokenDAO.resetToken(req.getHeader(HEADER_ACCESS_TOKEN));
    }
}
