package DAO;

import DB.PostgresDAOFactory;

public enum DBType {

    POSTGRESQL {
        @Override
        public DAOFactory getDAOFactory() {
            return new PostgresDAOFactory();
        }
    };

    public abstract DAOFactory getDAOFactory();

}
